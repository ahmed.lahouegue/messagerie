import {message} from "./message";

export class conversation {
    user : string;
    messages : message[];

    constructor(user:string, message:[]) {
        this.user = user;
        this.messages = message;
    }
}
