import {Component, NgModule, OnInit} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})


export class AppComponent implements OnInit {
  username = 'username';
  message = '';
  messages = [];


  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {

  }

  submit(): void {

  }

  dialog(): void {

  }
}
