import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {message} from "../model/message";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }

  getMessage(){
    return this.http.get<message[]>('http://localhost:3000/messages_sender')
  }
}
